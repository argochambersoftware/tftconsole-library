# README #
To start using this library, just make you sure that you are using:
#### Mcufriend 2.4'' LCD TFT Screen ####
(The one red maybe?)

Just use this example code to run your first project:


```
#!Arduino

#include "TFTConsole.h"

//Console acts for us as a bridge between us and the TFT library.
TFTConsole cout;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  cout.init();
}

int count = 0;
void loop() {
  // put your main code here, to run repeatedly:
  cout.println("Line "+(String)count);
  count += 1;
  delay(250);
}
```

* * *
### Mehods that you should use: ###

Just use
```
cout.print("some string");
```
and
```
cout.println("some string with newline");
```
to enjoy this simple Console Library.

For now, avoid using anything else...