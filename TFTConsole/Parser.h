/**
 * This is the parser for the console object.
 * Provides the color, given a word o sentence.
 */

#include "Colors.h"

#define MAX_COLORS 25

#include "Arduino.h"
 
#ifndef _PARSER_H_
#define _PARSER_H_

class Parser {
protected:
	uint16_t palette[MAX_COLORS];
	String keywords[MAX_COLORS];
	
	uint16_t defaultColor;
	
public:
	void setDefaultColor(uint16_t);
	uint16_t getColor(String key);
	void setKeyPair(const char * key, uint16_t color);
	Parser(void);
	
	void init(void);
	
};


#endif