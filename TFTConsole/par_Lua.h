/**
 * Lua syntax highlighter.
 */

#ifndef PARSER_LUA_H
#define PARSER_LUA_H

#include "Parser.h"
#include "Colors.h"

class pLua : public Parser {
public:
	void init(void){
		this->setKeyPair("if", BLUE);
		this->setKeyPair("then", BLUE);
		this->setKeyPair("else", BLUE);
		this->setKeyPair("return", BLUE);
		this->setKeyPair("end", BLUE);
		this->setKeyPair("false", BLUE);
		this->setKeyPair("true", BLUE);
		this->setKeyPair("elseif", BLUE);
		this->setKeyPair("do", BLUE);
		this->setKeyPair("while", BLUE);
		this->setKeyPair("not", BLUE);
		this->setKeyPair("or", BLUE);
		this->setKeyPair("and", BLUE);
		this->setKeyPair("for", BLUE);
		this->setKeyPair("in", BLUE);
		
		this->setKeyPair("pairs", CYAN);
		this->setKeyPair("ipairs", CYAN);
		
		/*
		for (int i=0; i<=25; i++){
			Serial.print(i);
			Serial.print(" - ");
			Serial.print(this->keywords[i]);
			Serial.print(" [");
			Serial.print(this->palette[i]);
			Serial.println("]");
		}
		*/
	};
};

static pLua Lua;


#endif