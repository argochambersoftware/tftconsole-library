/**
 * Console TFT Object declaration.
 */
 
//Uncoment the line below if you don't want to use parsers and want to save memory.
//Otherwise comment it if you want the parser running. ALPHA STAGE, NOT RUNNING!
#define _MUST_OVERRIDE_PARSER_SAVEPROGMEM_

#ifndef _TFT_CONSOLE_H_
#define _TFT_CONSOLE_H_

#include "Arduino.h"

//Argochamber's helper headers.
#include "Colors.h"
#include "Helpers.h"

#include "Sizes.h"

#include "Config.h"

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library

//This part takes care of what to be loaded in order to not waste memory.
#ifdef _MUST_OVERRIDE_PARSER_SAVEPROGMEM_
	#include "ParserEmpty.h"
#endif
#ifndef _MUST_OVERRIDE_PARSER_SAVEPROGMEM_
	#include "Parser.h" //This enables the use of syntax highlighters!
	#include "Parsers.h" //This holds the different parsers. Like Lua, C++, Java...
#endif

class TFTConsole {
	
private:
	String data;
	uint32_t fontSize;
	uint16_t textColor;
	uint16_t clearColor;
	int line; //Line where we print at.
	int pos; //Character where we print at.
	
	bool activeCheck;
	int maxCharLimit;
	int maxLineLimit;
	bool checkLimit(void);
	bool mustClearOnReset;
	
	void flush(void);

protected:
	Adafruit_TFTLCD screen;
	int calcCharLimit(void);
	int calcLineLimit(void);
	
	Parser par;

public:
	void init(void);
	void clear(void);
	void clear(uint16_t);
	
	void println(String);
	void print(String);
	void printAt(String,int,int);
	void printf(String);
	void printc(String,uint16_t);
	
	void setFontSize(uint32_t);
	void setClearColor(uint16_t);
	void setData(String);
	
	void setParser(Parser);
	
	void testCharMatrix(void);
	void testCharMatrix(const uint32_t);
	void fontTest(void);
	/*uint32_t getLineHeight(uint32_t);
	uint32_t getCharWidth(uint32_t);*/
	char randChar(void);
	void testLineCapacity(uint32_t);
	
	void setMustClearOnReset(bool);
	void reset(void);
	
	TFTConsole(void);//Primary constructor
	TFTConsole(uint16_t , uint16_t , uint16_t , uint16_t , uint16_t );
};


#endif

