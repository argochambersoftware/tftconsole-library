/**
 *  Main console file.
 * ~Argochamber
 * The class is "Broken" (That means that works, but only for '2' font size.)
 */

#include "TFTConsole.h"

#define FONT_OFFSET_HEIGHT 4
#define FONT_OFFSET_WIDTH 3

#include <stdlib.h>

static const char charset[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

int charsetLenght = sizeof(charset) - 1;


// the keywords LCD_CD, LCD_CD and so on are defined in "Config.h"

/**
 * Primary constructor.
 * Generates the tft controller and grabs-it.
 */
TFTConsole::TFTConsole(void) 
	: screen(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET){
	
	//Default print color. change with setTextColor(uint16_t color);
	this->textColor = WHITE;
	//Default clear color, like OpenGL when clearing the buffer.
	this->clearColor = BLACK;
	//Default font size.
	this->fontSize = 2;
	//This is the default charlimit per line.
	this->maxCharLimit = this->calcCharLimit();
	this->maxLineLimit = this->calcLineLimit();
	this->mustClearOnReset = true;
	this->activeCheck = true;
}

/**
 * This constructor is alternative, you can use it if you have to define other pins.
 * Not commonly used since this library was made for mcufriend.com TFT screen.
 */
TFTConsole::TFTConsole(uint16_t lcd_cs, uint16_t lcd_cd, uint16_t lcd_wr, uint16_t lcd_rd, uint16_t lcd_reset)
	: screen(lcd_cs, lcd_cd, lcd_wr, lcd_rd, lcd_reset){
	
	//Default print color. change with setTextColor(uint16_t color);
	this->textColor = WHITE;
	//Default clear color, like OpenGL when clearing the buffer.
	this->clearColor = BLACK;
	//Default font size.
	this->fontSize = 2;
	//This is the default charlimit per line.
	this->maxCharLimit = this->calcCharLimit();
	this->maxLineLimit = this->calcLineLimit();
	this->mustClearOnReset = true;
	this->activeCheck = true;
}

/**
 * Initializes the console screen and prints to the serial, if initialized.
 */
void TFTConsole::init(void) {
	Serial.println("Initializing console...");
	
	Serial.println("Pushing data to the screen...");
	
	this->screen.reset();
	uint16_t identifier = this->screen.readID();
	this->screen.begin(identifier);
	
	//Clear the screen
	this->clear();
	
	//Show in the screen the init text...
	this->screen.setCursor(0, 0);
	this->screen.setTextColor(WHITE);
	this->screen.setTextSize(1);
	this->screen.println("Initializing Screen service...");
	
	delay(500);
	
	this->clear();
	
	Serial.println("Done!");
	
}

/**
 * Clears the entire screen using the internal value "clearColor".
 * BLACK by default.
 */
void TFTConsole::clear(void){
	this->screen.fillScreen(this->clearColor);
}

/**
 * Clear the screen with a specific color.
 */
void TFTConsole::clear(uint16_t color){
	this->screen.fillScreen(color);
}

/**
 * The console "clear" function color.
 */
void TFTConsole::setClearColor(uint16_t color){
	this->clearColor = color;
}

/**
 * Flushes directly the data to the console screen.
 * @deprecated
 */
void TFTConsole::flush(void){}

/**
 * This sets the internal lexer.
 */
void TFTConsole::setParser(Parser p){
	this->par = p;
}
 

/**
 * prints the text at the current line and adds a new line.
 */
void TFTConsole::println(String line){
	int len = line.length();
	String fLine = line;
	
	//If full...
	if (this->activeCheck) {
		this->checkLimit();
	}
	
	//Calc print pos.
	this->screen.setCursor( 
		char_size.char_width[this->fontSize] * this->pos, 
		char_size.char_height[this->fontSize] * this->line
	);
	
	//Trim the string if exceeds the limit.
	if (len > this->maxCharLimit && len > 0) {
		fLine = line.substring(0, this->calcCharLimit());
	}
	
	//Now print the data...
	this->screen.setTextColor(WHITE);
	this->screen.setTextSize(this->fontSize);
	this->screen.println(fLine);
	//Because is a println, add a new line.
	this->line += 1;
	//As being a println, reset the last char pos.
	this->pos = 0;
}

/**
 * This version of print uses specific color.
 */
void TFTConsole::printc(String line, uint16_t color){
	int len = line.length();
	String fLine = line;
	
	//If full...
	if (this->activeCheck) {
		this->checkLimit();
	}
	
	//Calc print pos.
	this->screen.setCursor( 
		char_size.char_width[this->fontSize] * this->pos, 
		char_size.char_height[this->fontSize] * this->line
	);
	
	//Now, set the new char pos.
	this->pos += len;
	
	//Trim the string if exceeds the limit.
	if (len > this->maxCharLimit && len > 0) {
		fLine = line.substring(0, this->calcCharLimit());
		//Trim the string, get to another line...
		//Because is a println, add a new line.
		this->line += 1;
		//As being a println, reset the last char pos.
		this->pos = 0;
	}
	
	//Now print the data...
	this->screen.setTextColor(color);
	this->screen.setTextSize(this->fontSize);
	this->screen.println(fLine);
}

/**
 * Raw print of text at the current line.
 */
void TFTConsole::print(String line){
	this->printc(line, WHITE);
}
/**
 * This will print the same text but with syntax colored.
 */
void TFTConsole::printf(String text){
	int len = text.length();
	int size = 0;
	
	//Serial.println("Trimming...");
	
	int lastIndex = 0;
	
	while (lastIndex >= 0){
		int index = text.indexOf(" ", lastIndex+1);
		lastIndex = index;
		size++;
	}
	
	/*Serial.print("Measured chunks: ");
	Serial.println(size);
	
	Serial.println("Mapping:");
	
	for (int i=0; i < text.length(); i++){
		Serial.print(text[i]);
		if (i >= 10){
			Serial.print(" ");
		}
		Serial.print(" ");
	}
	Serial.println(" ");
	
	for (int i=0; i < text.length(); i++){
		Serial.print(i);
		Serial.print(" ");
	}
	Serial.println(" ");*/
	
	lastIndex = 0;
	
	//String finale[size];
	for (int i=0; i < size; i++){
		int index = text.indexOf(" ", lastIndex+1);
		
		//Do the string generation.
		String str;
		if (index == -1){
			index = len;
		}
		if (i > 0){
			str = text.substring(lastIndex+1,index);
		} else {
			str = text.substring(lastIndex,index);
		}
		
		/*
		Serial.print("Lead ");
		Serial.print(lastIndex);
		Serial.print(">>");
		Serial.print(index);
		Serial.print("; Chunk ");
		Serial.print(i+1);
		Serial.print(" -> '");
		Serial.print(str);
		Serial.println("'");
		*/
		
		lastIndex = index;
		
		//Append to the array.
		//finale[i] = str;
		
		this->printc(str+" ", this->par.getColor(str));
		
	}
	
	this->println("");
	
	//Serial.println("Done printf(...)");
}

/**
 * This method prints text at desired position.
 */
void TFTConsole::printAt(String text, int x, int y){
	this->screen.setCursor(x, y);
	this->screen.setTextColor(WHITE);
	this->screen.setTextSize(this->fontSize);
	this->screen.println(text);
}

/**
 * Sets directly the inner data value of the console.
 */
void TFTConsole::setData(String data){
	this->data = data;
}

/**
 * Set a font size.
 */
void TFTConsole::setFontSize(uint32_t size){
	this->fontSize = size;
}

/**
 * Prints a text showing the different font sizes.
 */
void TFTConsole::fontTest(void){
	for (int i = 1; i <= 6; i++){
		this->screen.setCursor(0, (char_size.char_width[i])*i);
		this->screen.setTextColor(WHITE);
		this->screen.setTextSize(i);
		this->screen.println( "Size" + (String)i );
	}
}

/**
 * This functions returns the line height, from a given font size (ie: 1).
 */
/*uint32_t TFTConsole::getLineHeight(uint32_t size){
	return size*size*FONT_OFFSET_HEIGHT;
}*/

/**
 * Same as above, but with the character height.
 */
/*uint32_t TFTConsole::getCharWidth(uint32_t size){
	return size*size*FONT_OFFSET_WIDTH;
}*/

/**
 * This function is protected by now. Calculates the character limit of the line.
 */
int TFTConsole::calcCharLimit(void){
	return 22; //Disabled for now... (live calc I mean.)
}

/**
 * This calculates the actual line limit.
 */
int TFTConsole::calcLineLimit(void){
	return 20; //Disabled for now, as above. Override in inherit classes.
}
 
/**
 * Generates a random charset.
 */
char TFTConsole::randChar(void){
    return charset[rand() % charsetLenght];
}

/**
 * This test was used to calibrate the getLineHeight and the getCharWidth methods.
 */
void TFTConsole::testCharMatrix(const uint32_t testSize){
	
	for (int i = 0; i <= 16; i++){
		for (int j = 0; j <= 16; j++){
			this->screen.setCursor( (char_size.char_width[testSize])*i, (char_size.char_height[testSize])*j);
			this->screen.setTextColor(WHITE);
			this->screen.setTextSize(testSize);
			this->screen.println(this->randChar());
		}
	}
}

/**
 * This is used to test, but with a constant instead of the variable input.
 */
void TFTConsole::testCharMatrix(void){
	const uint32_t testSize = 1;
	this->testCharMatrix(testSize);
}

/**
 * This aids the calibration of the line max character count.
 */
void TFTConsole::testLineCapacity(uint32_t size){
	
	uint16_t limit = 4;
	
	String str = "";
	
	for (int i = 0; i <= limit; i++){
		for (int j = 1; j <= 9; j++){
			str += (String)j;
		}
	}
	
	this->screen.setCursor( 0, 0 );
	this->screen.setTextColor(WHITE);
	this->screen.setTextSize(size);
	this->screen.println(str);
	
}

/**
 * This function checks if the console is "Full"
 */
bool TFTConsole::checkLimit(void){
	if (this->line >= this->maxLineLimit){
		this->reset();
		return true;
	} else {
		return false;
	}
}

/**
 * Reset the cursor of the console.
 */
void TFTConsole::reset(void){
	if (this->mustClearOnReset){
		this->clear();
	}
	this->pos = 0;
	this->line = 0;
	//Done resetting the console ^^
}

