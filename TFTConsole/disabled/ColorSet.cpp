/**
 * Color set.
 */
 
#include "ColorSet.h"

/**
 * Generates an empty collection of data.
 * Null color set.
 */
ColorSet::ColorSet(void){}

/**
 * This constructor pushes directly the given data to the storage of the color set.
 */
ColorSet::ColorSet(WordPair (&set)[MAX_COLORS])
	: data(set){ }

/**
 * Finds for the color with the specified name.
 */
uint16_t ColorSet::getColor(String keyword){
	uint16_t col = NULL;
	for (int i=0; i <= sizeof(MAX_COLORS); i++){
		if (this->data[i].iword != ""){
			if (this->data[i].iword == keyword){
				col = data[i].color;
			}
		}
	}
	return col;
}

/**
 * This puts at the last null entry the given pair.
 * Returns true if could be done, false otherwise.
 */
bool ColorSet::addPair(WordPair pair){
	for (int i=0; i <= MAX_COLORS; i++){
		if (this->data[i].iword == ""){
			this->data[i] = pair;
			return true;
		}
	}
	return false;
}

/**
 * Returns a raw data entry.
 */
WordPair ColorSet::getRaw(int index){
	return this->data[index];
}