/**
 * This is a simple word pair, pair of a word and a color.
 */

#ifndef _WORDPAIR_H_
#define _WORDPAIR_H_

#include "Arduino.h"

class WordPair {
public:
	String iword;
	uint16_t color;
	WordPair(){};
	WordPair(String s, uint16_t u) : iword(s), color(u) {};
};

#endif