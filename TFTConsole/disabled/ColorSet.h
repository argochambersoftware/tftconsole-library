/**
 * This is used as a hashmap for the parser.
 */

#ifndef _COLOR_SET_H_
#define _COLOR_SET_H_

#define MAX_COLORS 25

#include "Arduino.h"

#include "WordPair.h"

class ColorSet {
private:
	WordPair data [MAX_COLORS];
	
public:
	bool addPair(WordPair);
	uint16_t getColor(String keyword);
	WordPair getRaw(int);
	ColorSet(WordPair (&in)[MAX_COLORS]);
	ColorSet(void);
};

#endif