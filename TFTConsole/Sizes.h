/**
 * This library is the char size library, handles up to 8 sizes for now.
 */
 
#ifndef _SIZES_DEF_H_
#define _SIZES_DEF_H_

#define MAX_SIZES_HANDLE 9

struct Sizes{
	uint16_t char_width [MAX_SIZES_HANDLE];
	uint16_t char_height [MAX_SIZES_HANDLE];
	Sizes(void){
		//Size 1
		this->char_width[1] = 6;
		this->char_height[1]= 9;
		//Size 2
		this->char_width[2] = 12;
		this->char_height[2]= 17;
		//Size 3
		this->char_width[3] = 18;
		this->char_height[3]= 23;
		//Size 4
		this->char_width[4] = 22;
		this->char_height[4]= 32;
		//Size 5
		this->char_width[5] = 28;
		this->char_height[5]= 39;
		//Size 6
		this->char_width[6] = 34;
		this->char_height[6]= 44;
	};
};

static Sizes char_size;



#endif