/**
 * Parser.
 */

#include "Parser.h"

static const String DEFAULT_KEYWORD = "**default**";

Parser::Parser(void){
	this->defaultColor = WHITE;
}

/**
 * This method searches for the designed keyword in the array, stores the index and returns the color.
 * Returns the last matching color with the keyword. Otherwise, the default.
 */
uint16_t Parser::getColor(String keyword){
	
	uint16_t color = this->defaultColor;
	for (uint16_t i = 0; i <= MAX_COLORS; i++){
		if (this->keywords[i] == keyword){
			color = this->palette[i];
			break;
		}
	}
	return color;
}

/**
 * Sets manually the default color, if want to specify another than the color set defines, or is not defined.
 */
void Parser::setDefaultColor(uint16_t color){
	this->defaultColor = color;
}

/**
 * Sets a key-value for colors.
 * In fact, appends next to the last valid keypair a new key-value.
 */
void Parser::setKeyPair(const char * key, uint16_t color){
	
	for (uint16_t i=0; i <= MAX_COLORS; i++){
		if (this->keywords[i].length() <= 0){
			this->keywords[i] = String(key);
			this->palette[i] = color;
			break;
		}
	}
}

