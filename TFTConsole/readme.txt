Thanks for using Argochamber's Software libraries!
by SigmaSoldi3R

This library works as the Serial.print(); and Serial.println(); functions, also provides more functionality like printAt(x,y,text) functions.
For now, the library is in alpha development, so many functionalities may not work.
But you should be able to print and print lines to the LCD TFT Screen without problems.

Installation: Just drop the folder inside the .zip file in "documents/Arduino/libraries/".
Remember to include TFTConsole.h
