/**
 * This is an empty placeholder! does esentially nothing.
 */
 
#ifndef _PARSER_H_
#define _PARSER_H_

class Parser {
public:
	void setDefaultColor(uint16_t);
	uint16_t getColor(String key){
		return 0xFFFF;
	};
	void setKeyPair(const char * key, uint16_t color);
	void init(void);
};


#endif